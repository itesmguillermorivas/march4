package mx.itesm.march4;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements UsefulFragment.OnFragmentInteractionListener,
        RequestTask.RequestListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Toast.makeText(this, getString(R.string.message), Toast.LENGTH_SHORT).show();

        // add the fragment programmatically
        DoggoFragment doggo = new DoggoFragment();

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.container, doggo, "doggoFragment");
        transaction.commit();

        String jsonTest = "{'name':'Juan', 'age':20}";
        String jsonTest2 = "['Patricio', 'Quirino', 'Daniela']";
        String jsonTest3 = "{'name':'Francisco', 'grades':[35, 24, 100]}";
        String jsonTest4 = "[{'name': 'Emanuel', 'age':20}, {'name':'Carlos', 'age':20}, {'name':'Rosa', 'age':19}]";

        try {
            JSONObject jo = new JSONObject(jsonTest);

            Log.wtf("JSONTEST", jo.getString("name"));
            Log.wtf("JSONTEST", jo.getInt("age") + "");

            JSONArray ja = new JSONArray(jsonTest2);
            for(int i = 0; i < ja.length(); i++){

                Log.wtf("JSONTEST", ja.getString(i));
            }

            // example of object that contains an array
            JSONObject jo2 = new JSONObject(jsonTest3);
            Log.wtf("JSONTEST", jo2.getString("name"));
            JSONArray ja2 = jo2.getJSONArray("grades");
            for(int i = 0; i < ja2.length(); i++){
                Log.wtf("JSONTEST", ja2.getInt(i) + "");
            }

            // example of array of objects
            JSONArray ja3 = new JSONArray(jsonTest4);
            for(int i = 0; i < ja3.length(); i++){

                JSONObject current = ja3.getJSONObject(i);
                Log.wtf("JSONTEST", current.getString("name") + " " + current.getInt("age"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void swapFragments(View v){

        // change one fragment for the other
        FragmentManager manager = getSupportFragmentManager();
        Fragment f = manager.findFragmentByTag("doggoFragment");

        if(f != null){

            UsefulFragment f2 = UsefulFragment.newInstance("FRAGMENTITO");

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.remove(f);
            transaction.add(R.id.container, f2, "doggoFragment");
            transaction.commit();
        }
    }

    @Override
    public void fragmentCallback(String message) {

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void doRequest(View v) {

        RequestTask task = new RequestTask(this);
        task.execute("https://api.github.com/");
    }

    @Override
    public void requestDone(JSONObject jsonObject) {

        try {
            Toast.makeText(this, jsonObject.getString("current_user_url"), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void changeActivity(View v){

        Intent i = new Intent(this, NavigationActivity.class);
        startActivity(i);
    }
}
